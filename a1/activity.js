/* 
    A C T I V I T Y 

    1 Find users with letter 's' in their first name or 'd' in their last name.. SHow only the fName and lName and hide the _id field 

    2 find users who are from the HR department and their age is greater than or equal to 70

    3 find users with the letter 'e' in their first name and has an age of less than or equal to 30
*/


// 1
db.users.find(
    {
        $or: [ { fName: { $regex : "s", $options: '$i' }}, {lName: { $regex : "d", $options: '$i' }} ]
    },
    {
        _id : 0
    }
).pretty();

// 2 

db.users.find(
    {
        department : "HR",
        age : { $gte : 70}
    }
);

// 3 
db.users.find(
    {
        fName: { $regex : "e", $options: '$i' },
        age : { $lte : 30 }
    }
).pretty();

/* 
users collection

{
    "_id" : ObjectId("624edb1d09e0668c07dd1036"),
    "firstName" : "John",
    "lastName" : "Smith"
}


{
    "_id" : ObjectId("624edd1509e0668c07dd1037"),
    "firstName" : "Joe",
    "lastName" : "Doe"
}


{
    "_id" : ObjectId("624edd1509e0668c07dd1038"),
    "firstName" : "Jane",
    "lastName" : "Doe"
}

{
    "_id" : ObjectId("624ee53c09e0668c07dd103b"),
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "email" : "neil.armstrong@mail.com",
        "phone" : "09987654321"
    },
    "department" : "aeronautical"
}


{
    "_id" : ObjectId("624ee53c09e0668c07dd103a"),
    "firstName" : "Neil",
    "lastName" : "Diamond",
    "age" : 81.0,
    "contact" : {
        "email" : "neil.diamond@mail.com",
        "phone" : "09123456789"
    },
    "department" : "music"
}


*/