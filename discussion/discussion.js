/* 
query operators and field projection

limit the amount of field returned in data queries by using field projection techniques  

retrieve data from the database using diff query operators

define the differences between referencing data and embedded data

retrieve specific pieces of information from complex data structiures



    
 
 $lt 



*/

// Query operators - allow for more flexible query

// Operators 

// $gt / $gte operator 

    db.collectionName.find(
        {
            field : { $gt : value}
        }
    );
    db.collectionName.find(
        {
            field : { $gte : value}
        }
    );

    db.users.find(
        {
            age : { $gt : 50}
        }
    )

// $lt / $lte operator 

    db.users.find(
        {
            age : { $lt : 50}
        }
    )

    db.users.find(
        {
            age : { $lte : 50}
        }
    ).pretty();

// $ne operator 
db.users.find(
    {
        age : { $ne : 50}
    }
).pretty();

// $in 

db.users.find(
    {
        lastName : { $in : ["hawking", "Doe"]}
    }
).pretty();


// logical query operators 

// $or operator

db.users.find(
    {
        $or : [ { fName : "Neil"}, { age : "25" } ]
    }
).pretty(); // user either neil or age of 25 


// $and operator

db.users.find(
    {
        $and : [ { age : { $ne : 82}}, { age : { $ne : 76} } ]
    }
).pretty(); // user 

/* 
    Field Projection
        - Retrieving documents are common operation that we do and by 
        - default mongoDB queries return the whole document as a response 


    When dealing with complex data structure, there might be instances when fields are not usefuyl for the query that we are trying to acoomplish

    tp help with readability of the values returned, we can include/exclude fields from the response
*/


/*
    Inclusion
        - allows us to include / add specific fields only when retrieving documents.
        - the value provided is 1 to denote that the field is being included.
        
        S Y N T A X 

       S Y N T A X 
            db.users.find(
                {criteria},
                {field : 1 }
            )
*/


db.users.find(
    {
        fName : "Jane"
    },
    {
        fname : 1,
        lname  : 1,
        contact : 1 
    }
)

/*
    Exclusion 
        allows us to exclude/remove specific fields only when retrieving documents.
    
        the value provided is 0 to denote that the field is being included 

        S Y N T A X 
            db.users.find(
                {criteria},
                {field : 0 }
            )
*/

db.users.find(
    {
        fName : "Jane"
    },
    {
        contact : 0,
        department : 0
    }
).pretty();

/*
Supressing the id field 
    - allows us to explude the "_id" field when retrieving documents
    - when using field projection, field inclusion and explusion may not be used at the same time 
    - excluding the "_id" is the only exception to this rule 

    S Y N T A X 

        db.users.find(
            {criteria},
            {_id : 0}
        )
*/

db.users.find(
    {
        fName : "Jane"
    },
    {
        fName : 1,
        lName : 1,
        contact : 1,
        _id : 0
    }
).pretty();


/* 

user document >
    > contact document 
    > access document 

*/

/* 
 === embedded approach 
{
    _id : <objectID1>.
    username  : "123xyz",
    contact : {                                     
        phone : "123123123",
        email : "name@mail.com"
    }, 
    access : {
        level : 5,
        group : "dev"
    }
} 

 === reference approach 

// patron document
    {
        _id: "joe",
        name: "Joe Bookreader"
    }
// address documents
    {
        patron_id: "joe", // reference to patron document
        street: "123 Fake Street",
        city: "Faketon",
        state: "MA",
        zip: "12345"
    },

    {
        patron_id: "joe",
        street: "1 Some Other Street",
        city: "Boston",
        state: "MA",
        zip: "12345"
    }

*/

// embedded 

(
    {
        id : 12345,
        title : "Avengers",
        genre : "romans",
        reviews : [
            {
                name_r : "Jherson",
                rating : "5",
                comments : "lorem ipsum"
            },
            {
                name_r : "Ethan",
                rating : "5",
                comments : "lorem ipsum"
            }
        ]
        
    }
)

// reference 

/* 
    using smaller documents containig more frequently
*/
(
    {
        id : 12345,
        title : "Avengers",
        genre : "romans",
        reviews : [
            {
                reviewer_id : 123124-1,
            },
            {
                reviewer_id : 4123123-1,
            }
        ]
        
    }
)

/* 
    reviews document                                 
*/


(
    {
        name : "Hannah",
        courses : [
            {
                course_name : "HTML"
            },
            {
                course_name : "PHP"
            }
        ]
    },
)


//  case sensitive query

db.users.find(
    {
        fName : { $regex : 'N'}
    }
).pretty();

// case insensitive query 

db.users.find(
    {
        fName : { $regex : "j", $options : '$i'}
    }
)
